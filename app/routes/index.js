
const noteRoutes = require('./note_routes');
const categoryRoutes = require('./category_routes');

module.exports = function(app, db) {
  noteRoutes(app, db);
  categoryRoutes(app, db);
};
