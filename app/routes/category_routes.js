
var ObjectID = require('mongodb').ObjectID;

module.exports = function(app, db) {

  // ADD CATEGORY
  app.post('/categories/add', (req, res) => {

    const category = {
      uid: res.locals.uid,
      category: req.body.category,
      created_at: new Date()
    };

    db.collection("categories").insert(category, (err, results) => {
      if (err) {
        res.status(500).json({
          'status': 'ERROR',
          'message': 'An internal error has occurred'
        });
      }
      else {
        res.status(200).json({
          'status': 'SUCCESS',
          'category': results.ops[0]
        });
      }
    });

  });

  // GET ALL CATEGORIES
  app.post('/categories', (req, res) => {

    db.collection('categories').find({ uid: res.locals.uid }).toArray(function (err, categories) {
      // Internal error
      if (err) {
        res.status(500).json({
          'status': 'ERROR',
          'message': 'An internal error has occurred'
        });
      }
      else {
        res.status(200).json({
          'status': 'SUCCESS',
          'categories': categories
        });
      }
    });

  });

  // UPDATE CATEGORY
  app.put('/categories/:id', (req, res) => {

    var id = req.params.id;

    // Build our category with data from request
    const category = {
      uid: req.body.category.uid,
      category: req.body.category.category,
      created_at: req.body.category.created_at,
    };

    if(ObjectID.isValid(id)){
      const details = { '_id': ObjectID(id) };

      db.collection('categories').update(details, category, (err, result) => {
        // Internal error
        if (err) {
          res.status(500).json({
            'status': 'ERROR',
            'message': 'An internal error has occurred'
          });
        } else {
          // Category with (otherwise valid) ID not found
          if(result == null) {
            res.status(404).json({
              'status': 'ERROR',
              'message': 'Category with ID \'' + id + '\' could not be found'
            });
          }
          else {
            // Success
            res.status(200).json({
              'status': 'SUCCESS',
              'category': {
                ...category,
                _id: req.body.category._id,
              }
            });
          }
        }
      });
    }
    else {
      // Invalid ID passed
      res.status(404).json({
        'status': 'ERROR',
        'message': 'Category with ID \'' + id + '\' could not be found',
      });
    }

  });

  // DELETE CATEGORY
  app.delete('/categories/:id', (req, res) => {

    var id = req.params.id;

    if(ObjectID.isValid(id)){
      const details = { '_id': ObjectID(id) };

      db.collection('categories').remove(details, (err, item) => {
        // Internal error
        if (err) {
          res.status(500).json({
            'status': 'ERROR',
            'message': 'An internal error has occurred'
          });
        } else {
          // Category with (otherwise valid) ID not found
          if(item == null) {
            res.status(404).json({
              'status': 'ERROR',
              'message': 'Category with ID \'' + id + '\' could not be found'
            });
          }
          else {
            // Success
            res.status(200).json({
              'status': 'SUCCESS',
              'message': 'Category with ID \'' + id + '\' deleted'
            });
          }
        }
      });
    }
    else {
      // Invalid ID passed
      res.status(404).json({
        'status': 'ERROR',
        'message': 'Category with ID \'' + id + '\' could not be found',
      });
    }

  });

};
