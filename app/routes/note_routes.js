
var ObjectID = require('mongodb').ObjectID;

module.exports = function(app, db) {

  // CREATE NOTE
  app.post('/notes/add', (req, res) => {

    // TODO: if(type === 'textNote') etc.

    const note = {
      uid: res.locals.uid,
      title: req.body.note.title,
      note: req.body.note.text,
      type: req.body.type,
      category: req.body.category,
      created_at: new Date()
    };

    db.collection("notes").insert(note, (err, results) => {
      if (err) {
        res.status(500).json({
          'status': 'ERROR',
          'message': 'An internal error has occurred'
        });
      }
      else {
        res.status(200).json({
          'status': 'SUCCESS',
          'note': results.ops[0]
        });
      }
    });

  });


  // GET NOTE
  app.post('/notes/:id', (req, res) => {

    var id = req.params.id;

    if(ObjectID.isValid(id)){
      const details = { '_id': ObjectID(id) };

      db.collection('notes').findOne(details, (err, item) => {
        // Internal error
        if (err) {
          res.status(500).json({
            'status': 'ERROR',
            'message': 'An internal error has occurred'
          });
        } else {
          // Note with (otherwise valid) ID not found
          if(item == null) {
            res.status(404).json({
              'status': 'ERROR',
              'message': 'Note with ID \'' + id + '\' could not be found'
            });
          }
          else {
            // Success
            res.status(200).json({
              'status': 'SUCCESS',
              'note': item
            });
          }
        }
      });
    }
    else {
      // Invalid ID passed
      res.status(404).json({
        'status': 'ERROR',
        'message': 'Note with ID \'' + id + '\' could not be found',
      });
    }

  });

  // GET ALL NOTES
  app.post('/notes', (req, res) => {

    db.collection('notes').find({ uid: res.locals.uid }).toArray(function (err, notes) {
      // Internal error
      if (err) {
        res.status(500).json({
          'status': 'ERROR',
          'message': 'An internal error has occurred'
        });
      }
      else {
        res.status(200).json({
          'status': 'SUCCESS',
          'notes': notes
        });
      }
    });

  });


  // DELETE NOTE
  app.delete('/notes/:id', (req, res) => {

    var id = req.params.id;

    if(ObjectID.isValid(id)){
      const details = { '_id': ObjectID(id) };

      db.collection('notes').remove(details, (err, item) => {
        // Internal error
        if (err) {
          res.status(500).json({
            'status': 'ERROR',
            'message': 'An internal error has occurred'
          });
        } else {
          // Note with (otherwise valid) ID not found
          if(item == null) {
            res.status(404).json({
              'status': 'ERROR',
              'message': 'Note with ID \'' + id + '\' could not be found'
            });
          }
          else {
            // Success
            res.status(200).json({
              'status': 'SUCCESS',
              'message': 'Note with ID \'' + id + '\' deleted'
            });
          }
        }
      });
    }
    else {
      // Invalid ID passed
      res.status(404).json({
        'status': 'ERROR',
        'message': 'Note with ID \'' + id + '\' could not be found',
      });
    }

  });


  // TODO: Ensure this passes back the ID correctly, refer to update category
  // UPDATE NOTE
  app.put('/notes/:id', (req, res) => {

    var id = req.params.id;

    // TODO: if(req.body.type == "textNote") ... etc.
    const note = {
      uid: req.body.note.uid,
      title: req.body.note.title,
      note: req.body.note.note,
      type: req.body.note.type,
      category: req.body.note.category,
      created_at: req.body.note.created_at,
      updated_at: new Date(),
    };
    console.log(note);

    if(ObjectID.isValid(id)){
      const details = { '_id': ObjectID(id) };

      db.collection('notes').update(details, note, (err, result) => {
        // Internal error
        if (err) {
          res.status(500).json({
            'status': 'ERROR',
            'message': 'An internal error has occurred'
          });
        } else {
          // Note with (otherwise valid) ID not found
          if(result == null) {
            res.status(404).json({
              'status': 'ERROR',
              'message': 'Note with ID \'' + id + '\' could not be found'
            });
          }
          else {
            // Success
            res.status(200).json({
              'status': 'SUCCESS',
              'note': {
                ...note,
                _id: req.body.note._id,
              }
            });
          }
        }
      });
    }
    else {
      // Invalid ID passed
      res.status(404).json({
        'status': 'ERROR',
        'message': 'Note with ID \'' + id + '\' could not be found',
      });
    }

  });

};
