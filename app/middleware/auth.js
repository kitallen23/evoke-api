var admin = require("firebase-admin");
var serviceAccount = require('../../config/firebase-adminsdk-key.json');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://evoke-b5f6d.firebaseio.com"
});

module.exports = function(req, res, next) {
  var idToken = req.body.idToken;

  if(idToken) {
    admin.auth().verifyIdToken(idToken).then(function(decodedToken) {

      res.locals.uid = decodedToken.uid;   // SET THIS SOMEWHERE
      res.locals.isAuth = true;
      next();

    }).catch(function(error) {
      // res.locals.isAuth = false;
      // next();

      res.status(401).json({
        'status': 'ERROR',
        'message': 'Authorization failed. Please sign in again.'
      });
      return;
    });
  }
  else {
    // res.locals.isAuth = false;
    // next();

    res.status(401).json({
      'status': 'ERROR',
      'message': 'Authorization failed. Please sign in again.'
    });
  }
};
