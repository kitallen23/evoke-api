
## Setup
After cloning the repository, run:
> npm install

Then start the application with:
> npm start

## About
This API is built for the evoke note keeping web application, located [here](https://bitbucket.org/kitallen23/evoke).
