const express        = require('express');
const MongoClient    = require('mongodb').MongoClient;
const bodyParser     = require('body-parser');
const db             = require('./config/db');
const auth           = require('./app/middleware/auth');

const cors           = require('cors');

const app            = express();
const port           = 8000;

app.use(cors());
app.use(bodyParser.urlencoded({
  extended: true,
}));
app.use(bodyParser.json());
app.use(auth);    // Alternatively, app.use(auth, "/route/name")

MongoClient.connect(db.url, (err, database) => {

  if (err) return console.log(err);

  require('./app/routes')(app, database);

  app.listen(port, () => {
    console.log('Listening on port ' + port);
  });
})
